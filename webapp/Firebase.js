sap.ui.define([
	"sap/ui/model/json/JSONModel",
], function (JSONModel) {
	"use strict";

	// Firebase-config retrieved from the Firebase-console
	const firebaseConfig = {
		apiKey: "AIzaSyCY93y7qVsdd8vIIPtYt7szZSAgjO8WMJM",
		authDomain: "sap-firebase-connect-210cf.firebaseapp.com",
		projectId: "sap-firebase-connect-210cf",
		storageBucket: "sap-firebase-connect-210cf.appspot.com",
		messagingSenderId: "800178617780",
		appId: "1:800178617780:web:759af65eb4e9dbe0dbc6e1"
	};

	return {
		initializeFirebase: function () {
			// Initialize Firebase with the Firebase-config
			//firebase.initializeApp(firebaseConfig);
			if (!firebase.apps.length) {
				firebase.initializeApp(firebaseConfig);
			}

			// Create a Firestore reference
			const firestore = firebase.firestore();

			// Create a Authentication reference
			const fireAuth = firebase.auth();

			// Firebase services object
			const oFirebase = {
				firestore: firestore,
				fireAuth: fireAuth
			};

			// Create a Firebase model out of the oFirebase service object which contains all required Firebase services
			var fbModel = new JSONModel(oFirebase);

			// Return the Firebase Model
			return fbModel;
		}
	};
});