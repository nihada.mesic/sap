sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function (Controller, MessageBox, JSONModel, MessageToast) {
	"use strict";

	return Controller.extend("sap.firebase.SAP-Firebase-Connect.controller.Main", {

		collRefShipments: null,
		collRefUsers: null,
		collRefDetails: null,
		hemail: null,
		husername: null,
		hstaus: null,
		huser: null,
		approve: null,
		login: null,
		admin: false,
		global_id: null,
		onInit: function () {
			// Get the Firebase Model
			const firebaseModel = this.getView().getModel("firebase");
			// Create a Authentication reference
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");

			// Create a Firestore reference
			const firestore = this.getView().getModel("firebase").getProperty("/firestore");
			// Create a collection reference to the shipments collection
			this.collRefShipments = firestore.collection("aubih");
			this.collRefUsers = firestore.collection("users");

			// Initialize an array for the shipments of the collection as an object
			var oShipments = {
				shipments: [],
				currentShipment: {}
			};

			var oUsers = {
				users: [],
				currentUser: {}
			};


			var shipmentModel = new JSONModel(oShipments);
			this.getView().setModel(shipmentModel, "shipmentModel");

			var userModel = new JSONModel(oUsers);
			this.getView().setModel(userModel, "userModel");

			fireAuth.onAuthStateChanged(function (user) {
				console.log("pozvan");
				var husername = null;
				if (user !== null) {
					this.collRefUsers.get().then(
						function (collection) {
							var hemail = user.email;
							var userModel = this.getView().getModel("userModel");
							var userData = userModel.getData();
							var users = collection.docs.map(function (docUser) {
								if (docUser.data().email == hemail) {
									husername = docUser.data().username.toLowerCase();
								}
							});
							if (husername == "nadređeni") {
								//console.log("nadređeni");
								this.getUsers();
							}
							else if (husername == "admin") {
								//console.log("admin");
								this.getAdmin();
							} else if (husername == "radnik") {
								console.log("radnik");
								this.getShipments();
							}
						}.bind(this));

				} else {
					this.collRefShipments.get().then(
						function (collection) {
							var shipmentModel = this.getView().getModel("shipmentModel");
							var shipmentData = shipmentModel.getData();
							shipmentData.shipments = null;
							this.getView().byId("bussinessTripTable").getBinding("items").refresh();
						}.bind(this));
				}
			}.bind(this));

		},

		getShipments: function () {
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			fireAuth.onAuthStateChanged(function (user) {
				if (user != null) {
					var hemail = user.email;
					this.collRefShipments.get().then(
						function (collection) {
							var shipmentModel = this.getView().getModel("shipmentModel");
							var shipmentData = shipmentModel.getData();
							var shipments = collection.docs.map(function (docShipment) {
								if (docShipment.data().idmail == hemail) {
									console.log("heeemaiiiiil: ", hemail);
									return docShipment.data();
								}
							});
							shipmentData.shipments = shipments.filter(function (el) {
								return el != null;
							});
							this.getView().byId("bussinessTripTable").getBinding("items").refresh();
						}.bind(this));
				} else {
					this.collRefShipments.get().then(
						function (collection) {
							var shipmentModel = this.getView().getModel("shipmentModel");
							var shipmentData = shipmentModel.getData();
							shipmentData.shipments = null;
							this.getView().byId("bussinessTripTable").getBinding("items").refresh();
						}.bind(this));
				}
			}.bind(this));
		},

		getUsers: function () {
			this.collRefShipments.onSnapshot(function (snapshot) {
				// Get the shipment model
				var shipmentModel = this.getView().getModel("shipmentModel");
				// Get all the shipments
				var shipmentData = shipmentModel.getData();

				snapshot.docChanges().forEach(function (change) {
					var oShipment = change.doc.data();
					oShipment.id = change.doc.id;

					if (change.type === "added") {
						shipmentData.shipments.push(oShipment);
					}
					// Modified document (find its index and change current doc with the updated version)
					else if (change.type === "modified") {
						var index = shipmentData.shipments.map(function (shipment) {
							return shipment.id;
						}).indexOf(oShipment.id);
						shipmentData.shipments[index] = oShipment;
					}
					// Removed document (find index and remove it from the shipments array in the model)
					else if (change.type === "removed") {
						var index = shipmentData.shipments.map(function (shipment) {
							return shipment.id;
						}).indexOf(oShipment.id);
						shipmentData.shipments.splice(index, 1);
					}
				});
				this.getView().getModel("shipmentModel").refresh(true);
				this.getView().byId("bussinessTripTable").getBinding("items").refresh();
			}.bind(this));
		},

		getAdmin: function () {
			this.collRefShipments.onSnapshot(function (snapshot) {
				// Get the shipment model
				var shipmentModel = this.getView().getModel("shipmentModel");
				// Get all the shipments
				var shipmentData = shipmentModel.getData();

				snapshot.docChanges().forEach(function (change) {
					var oShipment = change.doc.data();
					oShipment.id = change.doc.id;
					//console.log("oShipment", oShipment.status);

					if (change.type === "added") {
						if (oShipment.status == "Approved")
							shipmentData.shipments.push(oShipment);
					}
					// Modified document (find its index and change current doc with the updated version)
					else if (change.type === "modified") {
						var index = shipmentData.shipments.map(function (shipment) {
							return shipment.id;
						}).indexOf(oShipment.id);
						if (oShipment.status == "Approved")
							shipmentData.shipments[index] = oShipment;
					}
					// Removed document (find index and remove it from the shipments array in the model)
					else if (change.type === "removed") {
						var index = shipmentData.shipments.map(function (shipment) {
							return shipment.id;
						}).indexOf(oShipment.id);
						if (oShipment.status == "Approved")
							shipmentData.shipments.splice(index, 1);
					}
				});
				this.getView().getModel("shipmentModel").refresh(true);
				this.getView().byId("bussinessTripTable").getBinding("items").refresh();
			}.bind(this));
		},
		onLogin: function () {
			this.login = "true";
			this._getDialog().open();
		},
		onOpenSignUpFragment: function () {
			this.approv = "true";
			this._getDialog().open();
		},

		onOpenAddBussinesTrip: function () {
			var husername = null;
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			fireAuth.onAuthStateChanged(function (user) {
				if (user != null) {
					this.collRefUsers.get().then(
						function (collection) {
							var hemail = user.email;
							var userModel = this.getView().getModel("userModel");
							var userData = userModel.getData();
							var users = collection.docs.map(function (docUser) {
								if (docUser.data().email == hemail) {
									husername = docUser.data().username.toLowerCase();
									//console.log(docUser.data().username.toLowerCase());
								}
							});
							if (husername == "radnik") {
								this.approve = "true";
								console.log(this.approve);
								this._getDialog().open();
							} else {
								alert("You can't add new bussiness trip, you aren't radnik!");
							}
						}.bind(this));
				}
			}.bind(this));

		},

		_getDialog: function () {
			if (this.approv == "true") {
				this._oDialog = sap.ui.xmlfragment("sap.firebase.SAP-Firebase-Connect.view.fragment.SignUp", this);
				this.getView().addDependent(this._oDialog);
				console.log("open sign up");
			} else if (this.approve == "true") {
				this._oDialog = sap.ui.xmlfragment("sap.firebase.SAP-Firebase-Connect.view.fragment.AddShipment", this);
				this.getView().addDependent(this._oDialog);
				console.log("open new bussiness");
			} else if (this.admin == "true") {
				this._oDialog = sap.ui.xmlfragment("sap.firebase.SAP-Firebase-Connect.view.fragment.ShowDetails", this);
				this.getView().addDependent(this._oDialog);
				console.log("open admin");
			} else if (this.login == "true") {
				this._oDialog = sap.ui.xmlfragment("sap.firebase.SAP-Firebase-Connect.view.fragment.Login", this);
				this.getView().addDependent(this._oDialog);
				console.log("open login");
			}
			this.approv = "false";
			this.approve = "false";
			this.login = "false";
			this.admin = "false";
			return this._oDialog;
		},

		onLogoutUser: function () {
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			fireAuth.signOut().then(function () {
				// Sign-out successful.
				this.onInit();
				alert('User logged out!');
			}).catch((error) => {
				// An error happened
				const errorCode = error.code;
				const errorMessage = error.message;
			});
			//location.reload();
			this.getShipments();
		},

		clearAddedShipment: function () {
			this.getView().getModel("shipmentModel").getContext("/currentShipment").getModel().getData().currentShipment = {};
			this.getView().getModel("shipmentModel").getContext("/currentShipment").getModel().setProperty("/selectedKey", "");
			this.getView().getModel("shipmentModel").refresh(true);
		},

		clearAddedUser: function () {
			this.getView().getModel("userModel").getContext("/currentUser").getModel().getData().currentShipment = {};
			this.getView().getModel("userModel").getContext("/currentUser").getModel().setProperty("/selectedKey", "");
			this.getView().getModel("userModel").refresh(true);
		},

		closeDialog: function (oEvent) {
			if (this.getView().getModel("userModel").getContext("/currentUser").getObject()) {
				//this.clearAddedUser(oEvent);
				//this.approv = "false";
				//this.login = "false";
				//this.approve = "false";
				//this.admin = "false";
				console.log("close sign up");
			} else if (this.admin == "true" ) {
				this.clearAddedShipment(oEvent);
				//this.admin = "false";
				console.log("close admin");
			} 
			console.log("zatvoriooo");
			this._getDialog().close();
			this.approv = "false";
			this.approve = "false";
			this.login = "false";
			this.admin = "false";
		},

		onSaveShipment: function (oEvent) {
			var oModel = oEvent.getSource().getModel("shipmentModel");
			var oShipment = oModel.getContext("/currentShipment").getObject();
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			fireAuth.onAuthStateChanged(function (user) {
				oShipment.idmail = user.email;
				oShipment.hotel = "NO";
				oShipment.insurance = "NO";
				oShipment.transport = "NO";
				oShipment.documentation = "NO";
				oShipment.payment = "NO";

				this.collRefShipments.add(oShipment).then(function (docShipment) {
					oShipment.id = docShipment.id;
					oModel.refresh(true);
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					this.clearAddedShipment(oEvent);
					this.closeDialog();
					console.log("Bussiness trip created successfully.")
				}.bind(this)).catch(function (error) {
					console.error("Error adding document: ", error);
				});
			}.bind(this));
		},

		onSaveUser: function (oEvent) {
			var oModel = oEvent.getSource().getModel("userModel");
			var oUser = oModel.getContext("/currentUser").getObject();
			console.log("ouser email: ", oUser.email);
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			fireAuth.createUserWithEmailAndPassword(oUser.email, oUser.password).then(function (userCredential) {
				// Signed in 
				const user = userCredential.user;
				alert('User created!')
			});
			this.collRefUsers.add(oUser).then(function (docUser) {
				oUser.id = docUser.id;
				oModel.refresh(true);
				this.clearAddedUser(oEvent);
				this.closeDialog();
				console.log("User created successfully.")
			}.bind(this)).catch(function (error) {
				console.error("Error adding document: ", error);
			});

		},

		onShipmentStatusSelect: function (oEvent) {
			var oShipment = oEvent.getSource().getModel("shipmentModel").getContext("/currentShipment").getObject();
			oShipment.status = oEvent.getSource().getSelectedItem().getKey();
		},

		onLoginUser: function (oEvent) {
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			var oModel = oEvent.getSource().getModel("userModel");
			var oUser = oModel.getContext("/currentUser").getObject();
			var email = oUser.email;
			var password = oUser.password;

			fireAuth.signInWithEmailAndPassword(email, password).then(function (userCredential) {
				// Signed in 
				const user = userCredential.user;
				alert('User logged in!');
				// ...
			});
		},

		checkDoneTransport: function (oEvent) {
			var bSelected = oEvent.getParameter('selected');
			if (bSelected) {
				console.log("i ovdje");
				this.collRefShipments.doc(this.global_id).update({ transport: "YES" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Transport updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating transport: ", error);
				});
			}
		},

		checkDoneTransport: function (oEvent) {
			var bSelected = oEvent.getParameter('selected');
			if (bSelected) {
				this.collRefShipments.doc(this.global_id).update({ transport: "YES" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Transport updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating transport: ", error);
				});
			} else {
				this.collRefShipments.doc(this.global_id).update({ transport: "NO" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Transport updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating transport: ", error);
				});
			}
		},

		checkDoneInsurance: function (oEvent) {
			var bSelected = oEvent.getParameter('selected');
			if (bSelected) {
				this.collRefShipments.doc(this.global_id).update({ insurance: "YES" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Insurance updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating insurance: ", error);
				});
			} else {
				this.collRefShipments.doc(this.global_id).update({ insurance: "NO" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Payment updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating insurance: ", error);
				});
			}
		},

		checkDoneDocumentation: function (oEvent) {
			var bSelected = oEvent.getParameter('selected');
			if (bSelected) {
				this.collRefShipments.doc(this.global_id).update({ documentation: "YES" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Documentation updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating documentation: ", error);
				});
			} else {
				this.collRefShipments.doc(this.global_id).update({ documentation: "NO" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Documentation updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating documentation: ", error);
				});
			}
		},

		checkDonePayment: function (oEvent) {
			var bSelected = oEvent.getParameter('selected');
			if (bSelected) {
				this.collRefShipments.doc(this.global_id).update({ payment: "YES" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Payment updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating payment: ", error);
				});
			} else {
				this.collRefShipments.doc(this.global_id).update({ payment: "NO" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Payment updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating payment: ", error);
				});
			}
		},

		onSaveHotel: function (oEvent) {
			var oModel = oEvent.getSource().getModel("shipmentModel");
			var oShipment = oModel.getContext("/currentShipment").getObject();
			console.log("hotel", oShipment.hotel);
			if (typeof (oShipment.hotel) === 'undefined') {
				console.log("proslo undefined");
				this.collRefShipments.doc(this.global_id).update({ hotel: "NO" }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Hotel updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating hotel: ", error);
				});
			} else {
				this.collRefShipments.doc(this.global_id).update({ hotel: oShipment.hotel }).then(function () {
					this.getView().byId("bussinessTripTable").getBinding("items").refresh();
					console.log("Hotel updated successfully.");
				}.bind(this)).catch(function (error) {
					console.error("Error updating hotel: ", error);
				});
			}
		},

		onClickApprove: function (oEvent) {
			const fireAuth = this.getView().getModel("firebase").getProperty("/fireAuth");
			var me = this;
			var shipmentId = oEvent.getSource().data().shipment.id;
			this.global_id = oEvent.getSource().data().shipment.id;
			var shipmentMail = oEvent.getSource().data().shipment.idmail;
			fireAuth.onAuthStateChanged(function (user) {
				var husername = null;
				if (user != null) {
					this.collRefUsers.get().then(
						function (collection) {
							var hemail = user.email;
							var userModel = this.getView().getModel("userModel");
							var userData = userModel.getData();
							var users = collection.docs.map(function (docUser) {
								if (docUser.data().email == hemail) {
									if (docUser.data().username.toLowerCase() == 'nadređeni') {
										husername = docUser.data().username;
										MessageBox.confirm("Are you sure you want to approve bussiness trip: " + shipmentMail + "?", {
											actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
											onClose: function (sAction) {
												if (sAction === "YES") {
													me.collRefShipments.doc(shipmentId).update({ status: "Approved" }).then(function () {
														me.getView().byId("bussinessTripTable").getBinding("items").refresh();
														console.log("Shipment: " + shipmentMail + " updated successfully.");
													}.bind(me)).catch(function (error) {
														console.error("Error removing document: " + shipmentMail + ": ", error);
													});
												} else {
													me.collRefShipments.doc(shipmentId).update({ status: "Not approved" }).then(function () {
														me.getView().byId("bussinessTripTable").getBinding("items").refresh();
														console.log("Shipment: " + shipmentMail + " updated successfully.");
													}.bind(me)).catch(function (error) {
														console.error("Error removing document: " + shipmentMail + ": ", error);
													});
												}
											}
										});
									} else if (docUser.data().username.toLowerCase() == 'admin') {
										//ispisat detalje o tom putu za korisnika
										me.admin = "true";
										me._getDialog().open();
									} else {
										alert("You don't have permission!");
									}
								}
							});
						}.bind(this));

				}
			}.bind(this));
		}

	});
});